
$(function() {



var productSlider = $('.product-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  swipe: false,
  asNavFor: '.product-slider__nav',
  responsive: [

    {
      breakpoint: 991,
      settings: {
        swipe: true,

      }
    }
  ]
});


$('.product-slider__nav').slick({
  slidesToShow: 30,
  slidesToScroll: 1,
  asNavFor: '.product-slider',
  dots: false,
  arrows: true,
  focusOnSelect: true,

  responsive: [
  {
    breakpoint: 1280,
    settings: {
      slidesToShow: 4,
    }
  },
  ]
});
























// Переключатель языка
$('.switch-btn').click(function (e, changeState) {
      if (changeState === undefined) {
          $(this).toggleClass('switch-on');
      }
      if ($(this).hasClass('switch-on')) {
          $(this).trigger('on.switch');
      } else {
          $(this).trigger('off.switch');
      }
  });

  $('.switch-btn').on('on.switch', function(){
      $('.language-rus').removeClass('active');
      $('.language-eng').addClass('active');
  });
  
  $('.switch-btn').on('off.switch', function(){
      $('.language-rus').addClass('active');
      $('.language-eng').removeClass('active');
  });




// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});









// Фильтр проектов

$('.project-filters__nav--show').click(function() {
  $(this).parent('.project-filters__nav').toggleClass('active');
})






//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});






// Projext scroll
$(".banner-project__nav").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;

    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 40}, 1500);
  });




// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();





 // Стилизация селектов
$('select').styler();


// Маска телефона
$(".phone-mask").mask("+7 (999) 999-99-99");




//  Слайдер новостей
var newsSlider = $('.news-slider').not('.slick-initialized').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  variableWidth: true,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        variableWidth: false,
      }
    },
  ]
});
$('.news-slider__prev').click(function(){
    $(newsSlider).slick("slickPrev")
});
$('.news-slider__next').click(function(){
    $(newsSlider).slick("slickNext")
});



//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrap').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});





// FansyBox
 $('.fancybox').fancybox({});




// FAQ
$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});











})